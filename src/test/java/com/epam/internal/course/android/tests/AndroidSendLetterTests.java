package com.epam.internal.course.android.tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.internal.course.android.data.DataRepository;
import com.epam.internal.course.android.data.RecipientLetterData;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

public class AndroidSendLetterTests extends TestRunner {

    @DataProvider
    public Object[][] getLetterData() {
        return new Object[][] { { DataRepository.get().getRecipientLetterData() }, };
    }

    @Epic("checkSendFunctionality")
    @Feature(value = "Check for sending a list of recipient")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for sending a list of recipient")
    @Story(value = "Check for sending a list of recipient")
    @Test(dataProvider = "getLetterData")
    public void checkSendFunctionality(RecipientLetterData recipientLetterData) throws InterruptedException {
        gmailBO
            .goToMainViewPage()
            .sendLetter(recipientLetterData);
        Assert.assertTrue(gmailBO.checkIsLetterSent(recipientLetterData), "the letter doesn't present in the Sent list");
    }

}
