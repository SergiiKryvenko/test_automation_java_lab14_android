package com.epam.internal.course.android.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.epam.internal.course.android.business.GmailBO;
import com.epam.internal.course.android.driver.AndroidDriverFactory;

public class TestRunner {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected GmailBO gmailBO;
    
    @AfterSuite
    public void driverQuit() {
        AndroidDriverFactory.driverQuit();
    }
    
    @BeforeSuite
    public void beforeSuite() {
    }

    @BeforeClass
    public void setUpBeforeClass() {
    }

    @AfterClass(alwaysRun = true)
    public void tearDownAfterClass() throws Exception {
        // TODO Logout
    }

    @BeforeMethod
    public void setUp() throws Exception {
        gmailBO = new GmailBO();
    }

    @AfterMethod
    public void tearDown(ITestResult result) throws Exception {
        if (!result.isSuccess()) {
            logger.warn("Error Test " + result.getName());
            // TODO Logout
        }
        logger.info("Done Test " + result.getName());
        // logout, delete cookie, delete cache
    }
}
