package com.epam.internal.course.android.pages;

import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.internal.course.android.driver.AndroidDriverFactory;
import com.epam.internal.course.android.utils.ExplicitWaitUtil;

public class BasePage {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected ExplicitWaitUtil wait;

    public BasePage() {
        PageFactory.initElements(AndroidDriverFactory.getDriver(), this);
        wait = new ExplicitWaitUtil(AndroidDriverFactory.getDriver());
    }
}