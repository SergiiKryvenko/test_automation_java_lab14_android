package com.epam.internal.course.android.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class ComposePage extends BasePage {

    @FindBy(id = "to")
    private WebElement recipientMailField;

    @FindBy(id = "subject")
    private WebElement subjectField;

    @FindBy(xpath = "//android.widget.EditText")
    private List<WebElement> bodyField;

    @FindBy(id = "send")
    private WebElement sendButton;

    @FindBy(className = "android.widget.Button")
    private WebElement gotItButton;

    @Step(value = "inputs a recipient email {0}")
    public ComposePage inputRecipientMail(String recipient) throws InterruptedException {
        logger.info(this.getClass().getSimpleName() + " start inputRecipientMail()");
        wait.elementToBeClickable(gotItButton);
        clickGotItButton();
        recipientMailField.sendKeys(recipient);
        logger.info("inputs a recipient email " + recipient);
        return this;
    }

    @Step(value = "inputs a subject for a letter {0}")
    public ComposePage inputSubject(String subject) {
        logger.info(this.getClass().getSimpleName() + " start inputSubject()");
        subjectField.sendKeys(subject);
        logger.info("inputs a subject for a letter " + subject);
        return this;
    }

    @Step(value = "inputs a body text {0}")
    public ComposePage inputBodyText(String text) {
        logger.info(this.getClass().getSimpleName() + " start inputBodyText()");
        bodyField.get(1).sendKeys(text);
        logger.info("inputs a body text " + text);
        return this;
    }

    @Step(value = "clicks on the Send button")
    public ComposePage clickSendButton() {
        logger.info(this.getClass().getSimpleName() + " start clickSendButton()");
        sendButton.click();
        logger.info("clicks on the Send button");
        return this;
    }

    @Step(value = "clicks on the GotIn button")
    private ComposePage clickGotItButton() {
        logger.info(this.getClass().getSimpleName() + " start clickGotItButton()");
        if (gotItButton.isDisplayed()) {
            gotItButton.click();
            logger.info("clicks on the GotIn button");
        }
        return this;
    }
}
