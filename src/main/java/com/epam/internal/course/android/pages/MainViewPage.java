package com.epam.internal.course.android.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.Step;

public class MainViewPage extends BasePage {

    @FindBy(id = "compose_button")
    private WebElement composeButton;
    
    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
    private WebElement menuDropBoxButton;
    
    @Step(value = "clicks on the Compose button")
    public MainViewPage clickComposeButton() {
        logger.info(this.getClass().getSimpleName() + " start clickComposeButton()");
        composeButton.click();
        logger.info("clicks on the Compose button");
        return this;
    }
    
    @Step(value = "clicks on the drop-box menu button")
    public MainViewPage clickMenuDropBoxButton() {
        logger.info(this.getClass().getSimpleName() + " start clickMenuDropBoxButton()");
        menuDropBoxButton.click();
        logger.info("clicks on the drop-box menu button");
        return this;
    }
}