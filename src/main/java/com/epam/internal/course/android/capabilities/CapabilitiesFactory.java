package com.epam.internal.course.android.capabilities;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.epam.internal.course.android.utils.ReadProjectProperties;

import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class CapabilitiesFactory {
    
    private static ReadProjectProperties readProjectProperties = new ReadProjectProperties();
    
    public static URL getAppiumServerURL() {
        try {
            return new URL(readProjectProperties.getURL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static DesiredCapabilities getCapabilities() {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, readProjectProperties.getPlatformName());
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, readProjectProperties.getDeviceName());
        desiredCapabilities.setCapability(MobileCapabilityType.UDID, readProjectProperties.getUDID());
        desiredCapabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, readProjectProperties.getAppPackage());
        desiredCapabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, readProjectProperties.getAppActivity());
        desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, readProjectProperties.getTimeOut());
        return desiredCapabilities;
    }
}
