package com.epam.internal.course.android.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.Step;

public class LeftMenuPage extends BasePage {

    @FindBy(xpath = "//android.widget.ListView/android.widget.LinearLayout")
    private List<WebElement> menuList;

    @Step(value = "clicks on the Primary in the menu")
    public LeftMenuPage clickPrimaryLink() {
        logger.info(this.getClass().getSimpleName() + " start clickPrimaryLink()");
        menuList.get(1).click();
        logger.info("clicks on the Primary in the menu");
        return this;
    }

    @Step(value = "clicks on the Sent in the menu")
    public LeftMenuPage clickSentLink() {
        logger.info(this.getClass().getSimpleName() + " start clickSentLink()");
        menuList.get(8).click();
        logger.info("clicks on the Sent in the menu");
        return this;
    }
}