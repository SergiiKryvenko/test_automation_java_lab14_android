package com.epam.internal.course.android.driver;

import java.util.concurrent.TimeUnit;

import com.epam.internal.course.android.capabilities.CapabilitiesFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class AndroidDriverFactory {

    private static AndroidDriver<MobileElement> driver;

    private AndroidDriverFactory() {
    }

    public static AndroidDriver<MobileElement> getDriver() {
        if (driver == null) {
            driver = new AndroidDriver<>(CapabilitiesFactory.getAppiumServerURL(), CapabilitiesFactory.getCapabilities());
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static void driverQuit() {
        driver.quit();
    }
}
