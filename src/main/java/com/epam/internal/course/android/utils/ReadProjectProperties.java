package com.epam.internal.course.android.utils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReadProjectProperties {

    private Properties properties;
    private final String PROPERTIES_PATH = "src\\main\\resources\\project.properties";

    public ReadProjectProperties() {
        properties = new Properties();
        try {
            properties.load(new FileReader(PROPERTIES_PATH));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPlatformName() {
        return properties.getProperty("platform.name");
    }

    public String getDeviceName() {
        return properties.getProperty("device.name");
    }

    public String getUDID() {
        return properties.getProperty("udid");
    }
    
    public String getAppActivity() {
        return properties.getProperty("app.activity");
    }
    
    public String getAppPackage() {
        return properties.getProperty("app.package");
    }
    
    public String getTimeOut() {
        return properties.getProperty("time.out");
    }
    
    public String getURL() {
        return properties.getProperty("url");
    }
}
