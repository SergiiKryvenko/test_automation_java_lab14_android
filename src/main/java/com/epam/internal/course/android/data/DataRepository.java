package com.epam.internal.course.android.data;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * DataRepository class which holds different sets of data.
 * @author SergiiK
 */
public final class DataRepository {
    private static volatile DataRepository instance = null;

    private DataRepository() {
    }

    public static DataRepository get() {
        if (instance == null) {
            synchronized (DataRepository.class) {
                if (instance == null) {
                    instance = new DataRepository();
                }
            }
        }
        return instance;
    }

    /**
     * Getting the data for letter.
     * @return RecipientLetterData class.
     */
    public RecipientLetterData getRecipientLetterData() {
        String email = "test_email@mail.com";
        //
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String subject = "test_subject: " + formatter.format(date);
        //
        String body = "test body";
        return new RecipientLetterData(email, subject, body);
    }

}