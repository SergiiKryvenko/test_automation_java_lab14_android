package com.epam.internal.course.android.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.Step;

public class WelcomePage extends BasePage{

    @FindBy(id = "welcome_tour_got_it")
    private WebElement gotItButton;
    
    @FindBy(id = "action_done")
    private WebElement takeMeToGmailButton;
    
    @FindBy(id = "account_address")
    private WebElement accountAddress;
    
    @Step(value = "clicks on the GotIt button")
    public WelcomePage clickGotItButton() {
        logger.info(this.getClass().getSimpleName() + " start clickGotItButton()");
        gotItButton.click();
        logger.info("clicks on the GotIt button");
        return this;
    }
    
    @Step(value = "clicks on the TakeMeToGmail button")
    public WelcomePage clickTakeMeToGmailButton() {
        logger.info(this.getClass().getSimpleName() + " start clickTakeMeToGmailButton()");
        wait.visibilityOfWebElement(accountAddress);
        takeMeToGmailButton.click();
        logger.info("clicks on the TakeMeToGmail button");
        return this;
    }
}
