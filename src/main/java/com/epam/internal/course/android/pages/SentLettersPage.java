package com.epam.internal.course.android.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.Step;

public class SentLettersPage extends BasePage {
    
    @FindBy(xpath = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup")
    private List<WebElement> setLettersList;
    
    @Step(value = "gets a list of subject of letters")
    public List<String> getSentLetterSubjectList(){
        logger.info(this.getClass().getSimpleName() + " start getSentLetterSubjectList()");
        List<String> listSentLetterSubjects = new ArrayList<String>();
        for (WebElement listLetters : setLettersList) {
            String subject = listLetters.findElements(By.xpath("//android.widget.TextView")).get(2).getText();
            if(subject.length() !=0) {
                listSentLetterSubjects.add(subject);
            }
        }
        logger.info("gets a list of the subject of letters");
        return listSentLetterSubjects;
    }
    
    @Step(value = "checks if the subject exists in the list")
    public boolean isSentLetterDisplayed(String subject) {
        logger.info(this.getClass().getSimpleName() + " start isSentLetterDisplayed()");
        for (String listSubject : getSentLetterSubjectList()) {
            if(listSubject.equalsIgnoreCase(subject)) {
                logger.info("the subject exists in the list");
                return true;
            }
        }
        logger.info("the subject doesn't exist in the list");
        return false;
    }
}