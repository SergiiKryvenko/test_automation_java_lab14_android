package com.epam.internal.course.android.business;

import com.epam.internal.course.android.data.RecipientLetterData;
import com.epam.internal.course.android.pages.ComposePage;
import com.epam.internal.course.android.pages.LeftMenuPage;
import com.epam.internal.course.android.pages.MainViewPage;
import com.epam.internal.course.android.pages.SentLettersPage;
import com.epam.internal.course.android.pages.WelcomePage;

import io.qameta.allure.Step;

public class GmailBO {
    
    private WelcomePage welcomePage;
    private MainViewPage mainViewPage;
    private LeftMenuPage leftMenuPage;
    private ComposePage composePage;
    private SentLettersPage sentLettersPage;
    
    public GmailBO() {
        welcomePage = new WelcomePage();
        mainViewPage = new MainViewPage();
        leftMenuPage = new LeftMenuPage();
        composePage = new ComposePage();
        sentLettersPage = new SentLettersPage();
    }
    
    @Step(value = "skip the Welcome page and go to the Main page")
    public GmailBO goToMainViewPage() {
        welcomePage
            .clickGotItButton()
            .clickTakeMeToGmailButton();
        return this;
    }
    
    @Step(value = "start send the letter to the recipient {0}")
    public GmailBO sendLetter(RecipientLetterData letterData) throws InterruptedException {
        mainViewPage.clickComposeButton();
        composePage
            .inputRecipientMail(letterData.getEmail())
            .inputSubject(letterData.getSubject())
            .inputBodyText(letterData.getBody())
            .clickSendButton();
        return this;
    }
    
    @Step(value = "start checking if our letter {0} exists on the Sent page")
    public boolean checkIsLetterSent(RecipientLetterData letterData) {
        mainViewPage.clickMenuDropBoxButton();
        leftMenuPage.clickSentLink();
        return sentLettersPage.isSentLetterDisplayed(letterData.getSubject());
    }

}
